const express = require('express');
const app = express();
const postman = require('./postman');
const secretary = require('./secretary');
const config = require('./config');
const fetch = require('node-fetch');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.json({msg:"Hello"});
});

app.use('/public', express.static('./public'));

app.post('/postoffice', async (req, res) => {
    try {
        if(req.body.permit === "recaptcha"){
            console.log("Permig: recaptcha"); 
            const params = {
                secret: config.recaptchaSecret,
                response: req.body["g-recaptcha-response"]
            };
            let url = 'https://www.google.com/recaptcha/api/siteverify?secret='
                +params.secret
                +'&response='
                +params.response
            let verifyRes = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
            });
            if(verifyRes.status != 200) res.status(500).json({});
            verifyRes = await verifyRes.json();
            console.log(verifyRes);
            if(verifyRes.success && await postman.sendMail(req.body, "Guest")) {
                res.json({ message: "Mail send" });
            } else {
                res.status(500).json({ err: "Could not send mail"});
            }
        } else throw "Permission denied";
    } catch (err) {
        res.status(500).json({err});
    }
});

app.get('/mailbox', async (req, res) => {
    let dump = false;
    if(dump = await secretary.dumpMails()) res.json({ok: true, dump: dump});
    else res.status(500).json({});
})

module.exports = app;