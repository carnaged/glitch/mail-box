const mailModel = require('./mail-model');

module.exports = {
    sendMail: async function(content, from){
        let mail = new mailModel({
            content, from
        });
        try {
            await mail.save();
            return true;
        } catch (err) {
            return false;
        }
    }
}