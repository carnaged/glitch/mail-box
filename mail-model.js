const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mailSchema = Schema({
    content: Object,
    from: String,
    date: {
        type: Date,
        required: true,
        default: Date.now()
    }
});

const mailModel = mongoose.model('mail', mailSchema, 'mails');

module.exports = mailModel;